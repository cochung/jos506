// Simple command-line kernel monitor useful for
// controlling the kernel and exploring the system interactively.

#include <inc/stdio.h>
#include <inc/string.h>
#include <inc/memlayout.h>
#include <inc/assert.h>
#include <inc/x86.h>

#include <kern/console.h>
#include <kern/monitor.h>
#include <kern/dwarf.h>
#include <kern/kdebug.h>
#include <kern/dwarf_api.h>

#define CMDBUF_SIZE	80	// enough for one VGA text line


struct Command {
	const char *name;
	const char *desc;
	// return -1 to force monitor to exit
	int (*func)(int argc, char** argv, struct Trapframe* tf);
};

static struct Command commands[] = {
	{ "help", "Display this list of commands", mon_help },
	{ "kerninfo", "Display information about the kernel", mon_kerninfo },
	 {"backtrace", "backtrace_function", mon_backtrace},

};
#define NCOMMANDS (sizeof(commands)/sizeof(commands[0]))

/***** Implementations of basic kernel monitor commands *****/

int
mon_help(int argc, char **argv, struct Trapframe *tf)
{
	int i;

	for (i = 0; i < NCOMMANDS; i++)
		cprintf("%s - %s\n", commands[i].name, commands[i].desc);
	return 0;
}

int
mon_kerninfo(int argc, char **argv, struct Trapframe *tf)
{
	extern char _start[], entry[], etext[], edata[], end[];

	cprintf("Special kernel symbols:\n");
	cprintf("  _start                  %08x (phys)\n", _start);
	cprintf("  entry  %08x (virt)  %08x (phys)\n", entry, entry - KERNBASE);
	cprintf("  etext  %08x (virt)  %08x (phys)\n", etext, etext - KERNBASE);
	cprintf("  edata  %08x (virt)  %08x (phys)\n", edata, edata - KERNBASE);
	cprintf("  end    %08x (virt)  %08x (phys)\n", end, end - KERNBASE);
	cprintf("Kernel executable memory footprint: %dKB\n",
		ROUNDUP(end - entry, 1024) / 1024);
	return 0;
}

int mon_backtrace(int argc, char **argv, struct Trapframe *tf)
{

    //     Stack backtrace:
    //   rbp 00000000f0111f20  rip 00000000f01000be
    //   rbp 00000000f0111f40  rip 00000000f01000a1
    // Your code here.
    cprintf("Stack backtrace:\n");
    uint64_t return_rbp_value = read_rbp();
    uint64_t *rbp = (uint64_t *)return_rbp_value;
    uintptr_t rip;
    read_rip(rip);

    // struct Ripdebuginfo *info = (struct Ripdebuginfo *)calloc(sizeof(struct Ripdebuginfo));
    struct Ripdebuginfo info;

    while (1)
    {

        cprintf(" rbp %016x rip %016x\n", (uint64_t)rbp, rip);

        int status = debuginfo_rip(rip, &info);
        if (status == 0)
        {

            // cprintf("rip file %s\n", info.rip_file);
            // cprintf("rip_line %d\n", info.rip_line);
            // cprintf("fn name %s, fn_namelen %d\n", info.rip_fn_name);
            // cprintf("rip_fn_addr %016x \n", info.rip_fn_addr);
            // cprintf("fn_narg %d \\n", info.rip_fn_narg);
            // cprintf("offset_fn_arg %016x, \n\n\n", info.offset_fn_arg);
            // cprintf("offset_fn_arg %016x, \n\n\n", info.offset_fn_arg);
            // cprintf("offset_fn_arg %016x, \n\n\n", info.offset_fn_arg);

            uint64_t fn_addr = info.rip_fn_addr;
            uint64_t offset = rip - fn_addr;
            int fn_narg = info.rip_fn_narg;
            cprintf("      %s:%d: %s+%016x args:%d  ", info.rip_file, info.rip_line, info.rip_fn_name, offset, fn_narg);



            int argc = 1;

            for (; argc <= fn_narg; argc++)
            {

                // int i = 0;
                // for (; i < fn_narg; i++)
                // {
                //     uint64_t *of = info.offset_fn_arg;
                //     uint64_t of2 = of[i];
                //     cprintf("\n!!!!\n %016x, \n!!!\n", of2);
                // }

                uint64_t argument = *(rbp - argc);

                cprintf("  %016x ", argument >> 32);
                // cprintf("  %d\n", argument >> 32);


            }



            cprintf("\n");
        }
        else
        {
            cprintf("error debuginfo_rip  \n");
        }

        uint64_t *rip_addr = rbp + 1;
				if(rip_addr !=0)
				{
					rip = *rip_addr;
				}

				if(*rbp !=0)
				{
					rbp = (uint64_t *)(*rbp);
				}
				else{
					break;
				}


        // struct Ripdebuginfo
        // {
        //     const char *rip_file; // Source code filename for RIP
        //     int rip_line;         // Source code linenumber for RIP

        //     const char *rip_fn_name; // Name of function containing RIP
        //     //  - Note: not null terminated!
        //     int rip_fn_namelen;          // Length of function name
        //     uintptr_t rip_fn_addr;       // Address of start of function
        //     int rip_fn_narg;             // Number of function arguments
        //     int size_fn_arg[10];         // Sizes of each of function arguments
        //     uintptr_t offset_fn_arg[10]; // Offset of each of function arguments (from CFA)
        //     Dwarf_Regtable reg_table;
        // };

        // debuginfo_rip(uintptr_t addr, struct Ripdebuginfo * info)
    }
    return 0;
}


/***** Kernel monitor command interpreter *****/

#define WHITESPACE "\t\r\n "
#define MAXARGS 16

static int
runcmd(char *buf, struct Trapframe *tf)
{
	int argc;
	char *argv[MAXARGS];
	int i;

	// Parse the command buffer into whitespace-separated arguments
	argc = 0;
	argv[argc] = 0;
	while (1) {
		// gobble whitespace
		while (*buf && strchr(WHITESPACE, *buf))
			*buf++ = 0;
		if (*buf == 0)
			break;

		// save and scan past next arg
		if (argc == MAXARGS-1) {
			cprintf("Too many arguments (max %d)\n", MAXARGS);
			return 0;
		}
		argv[argc++] = buf;
		while (*buf && !strchr(WHITESPACE, *buf))
			buf++;
	}
	argv[argc] = 0;

	// Lookup and invoke the command
	if (argc == 0)
		return 0;
	for (i = 0; i < NCOMMANDS; i++) {
		if (strcmp(argv[0], commands[i].name) == 0)
			return commands[i].func(argc, argv, tf);
	}
	cprintf("Unknown command '%s'\n", argv[0]);
	return 0;
}

void
monitor(struct Trapframe *tf)
{
	char *buf;

	cprintf("Welcome to the JOS kernel monitor!\n");
	cprintf("Type 'help' for a list of commands.\n");


	while (1) {
		buf = readline("K> ");
		if (buf != NULL)
			if (runcmd(buf, tf) < 0)
				break;
	}
}
